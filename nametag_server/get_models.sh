#!/bin/sh

set -e

dir=$(dirname $0)

for url in \
  https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11858/00-097C-0000-0023-7D42-8/czech-cnec-140304.zip \
  https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3118/english-conll-140408.zip
do
  fname=${url##*/}
  curl $url -o $dir/$fname
  unzip -d $dir $dir/$fname
  rm $dir/$fname
done
