#!/bin/sh

dir=$(dirname $0)

$dir/nametag_server "$@" \
 cs $dir/czech-cnec-140304/czech-cnec2.0-140304.ner "http://ufal.mff.cuni.cz/nametag/users-manual#czech-cnec_acknowledgements" \
 en $dir/english-conll-140408/english-conll-140408.ner "http://ufal.mff.cuni.cz/nametag/users-manual#english-conll_acknowledgements" \
