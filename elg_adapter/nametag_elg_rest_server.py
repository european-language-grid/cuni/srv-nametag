#!/usr/bin/env python3
import argparse
import json
import re

import aiohttp
import aiohttp.web

class NameTagElgAdapter:
    class ProcessingError(Exception):
        def __init__(self, code, text, *params):
            self.code = code
            self.text = text
            self.params = params

        @staticmethod
        def InternalError(text):
            return NameTagElgAdapter.ProcessingError("elg.service.internalError", "Internal error during processing: {0}", text)

        @staticmethod
        def InvalidRequest():
            return NameTagElgAdapter.ProcessingError("elg.request.invalid", "Invalid request message")

        @staticmethod
        def TooLarge():
            return NameTagElgAdapter.ProcessingError("elg.request.too.large", "Request size too large")

        @staticmethod
        def UnsupportedMime(mime):
            return NameTagElgAdapter.ProcessingError("elg.request.text.mimeType.unsupported", "MIME type {0} not supported by this service", mime)

        @staticmethod
        def UnsupportedType(request_type):
            return NameTagElgAdapter.ProcessingError("elg.request.type.unsupported", "Request type {0} not supported by this service", request_type)


    def __init__(self, host, port, max_request_size, session):
        self.host = host
        self.port = port
        self.max_request_size = max_request_size
        self.session = session

    def _error_message(self, message):
        return {
            "failure": {
                "errors": [{
                    "code": message.code,
                    "text": message.text,
                    "params": message.params,
                }]
            }
        }

    def _annotation_message(self, annotations):
        return {
            "response": {
                "type": "annotations",
                "annotations": {
                    "nametag/{}".format(key): annotations[key] for key in annotations
                }
            }
        }

    async def _parse_elg_request(self, request):
        try:
            if request.content_type == "text/plain":
                return await request.text()
            elif request.content_type == "application/json":
                body = await request.text()
                try:
                    request = json.loads(body)
                except json.JSONDecodeError:
                    raise self.ProcessingError.InvalidRequest()

                if not isinstance(request, dict):
                    raise self.ProcessingError.InvalidRequest()

                if request.get("type", None) != "text":
                    raise self.ProcessingError.UnsupportedType(request.get("type", "<missing>"))

                if request.get("mimeType", "text/plain") != "text/plain":
                    raise self.ProcessingError.UnsupportedMime(request["mimeType"])

                return request.get("content", "")
            else:
                raise self.ProcessingError.UnsupportedMime(request.content_type)
        except aiohttp.web.HTTPRequestEntityTooLarge:
            raise self.ProcessingError.TooLarge()

    async def _run_nametag(self, text, model):
        # Construct request
        payload = aiohttp.FormData({
            "data": text,
            "model": model,
            "input": "untokenized",
            "output": "xml",
        })._gen_form_data() # This is awful, but we want to force multipart/form-data mime.
        # The multipart/form-data is more space efficient, because it uses UTF-8 encoding
        # directly, instead of escaping used by x-www-form-urlencoded.

        # Make sure the request is not too large
        if payload.size > args.max_request_size:
            raise self.ProcessingError.TooLarge()

        # Perform the request
        try:
            async with self.session.post("http://{}:{}/recognize".format(self.host, self.port), data=payload) as request:
                response = await request.json()
                response = response["result"]
        except:
            raise self.ProcessingError.InternalError("Cannot run the NameTag service")

        return response

    def _xml_to_elg_annotations(self, xml):
        entities = {}

        # Remove the sentence and token annotations
        xml = xml.replace("<sentence>", "").replace("</sentence>", "")
        xml = xml.replace("<token>", "").replace("</token>", "")

        try:
            # Extract character indices of the entities
            xml_offset, text_offset, stack = 0, 0, []
            next_amp, next_lt = xml.find("&"), xml.find("<")
            while next_amp != -1 or next_lt != -1:
                if next_amp != -1 and (next_lt == -1 or next_amp < next_lt):
                    text_offset += next_amp - xml_offset + 1
                    xml_offset = xml.index(";", next_amp) + 1
                else:
                    text_offset += next_lt - xml_offset
                    if xml.startswith("/ne>", next_lt + 1):
                        start, ne_type = stack.pop()
                        if ne_type not in entities:
                            entities[ne_type] = []
                        entities[ne_type].append({"start": start, "end": text_offset, "features": {}})
                    else:
                        assert xml.startswith('ne type="', next_lt + 1)
                        stack.append((text_offset, xml[next_lt + 10:xml.index('"', next_lt + 10)]))
                    xml_offset = xml.find(">", next_lt) + 1

                if next_amp != -1 and next_amp < xml_offset:
                    next_amp = xml.find("&", xml_offset)
                if next_lt != -1 and next_lt < xml_offset:
                    next_lt = xml.find("<", xml_offset)
            assert len(stack) == 0
        except:
            raise self.ProcessingError.InternalError("Cannot parse NameTag service XML response")

        return entities

    def _json_response(self, json):
        return aiohttp.web.json_response(json, headers={"Access-Control-Allow-Origin": "*"})

    async def process_elg_request(self, request):
        model = request.match_info.get("model", "")
        try:
            content = await self._parse_elg_request(request)
            xml = await self._run_nametag(content, model)
            annotations = self._xml_to_elg_annotations(xml)
            return self._json_response(self._annotation_message(annotations))

        except self.ProcessingError as e:
            return self._json_response(self._error_message(e))
        except:
            return self._json_response(self._error_message(self.ProcessingError.InternalError("Internal service error")))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--max_request_size", default=1000000, type=int, help="Maximum request size")
    parser.add_argument("--port", default=8080, type=int, help="Port where to start the REST server")
    parser.add_argument("--nametag_host", default="localhost", type=str, help="Host of running NameTag server")
    parser.add_argument("--nametag_port", default=8002, type=int, help="Port of running NameTag server")
    args = parser.parse_args()

    async def server(args):
        session = aiohttp.ClientSession()
        adapter = NameTagElgAdapter(args.nametag_host, args.nametag_port, args.max_request_size, session)

        # Allow triple the limit in getting the request -- it might use %XY encoding.
        server = aiohttp.web.Application(client_max_size=3 * args.max_request_size)
        server.add_routes([aiohttp.web.post("/nametag/{model}", adapter.process_elg_request)])
        server.on_cleanup.append(lambda app: session.close())

        return server

    aiohttp.web.run_app(server(args), port=args.port)
