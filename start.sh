#!/bin/sh

dir=$(dirname $0)

$dir/nametag_server/start.sh 8002 --daemon --log_file "" --max_connections 256 --max_request_size 1024
python3 $dir/elg_adapter/nametag_elg_rest_server.py --port=8080 --max_request_size=1024000 --nametag_host=localhost --nametag_port=8002
