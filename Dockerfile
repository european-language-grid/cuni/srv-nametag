# Use an official Python 3.7 runtime with slim Debian Buster
FROM python:3.7-slim-buster

# Set the working directory to /nametag
WORKDIR /nametag

# Copy the required files to /nametag
COPY LICENSE README.md start.sh /nametag/
COPY elg_adapter /nametag/elg_adapter/
COPY nametag_server /nametag/nametag_server/

# Download the models
RUN apt-get update
RUN apt-get install -y curl unzip
RUN /nametag/nametag_server/get_models.sh

# Install required Python packages
RUN pip3 install aiohttp

# Expose the service port
EXPOSE 8080

# Run the service when the container launches
CMD ["/bin/sh", "/nametag/start.sh"]
